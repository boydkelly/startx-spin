%packages
@admin-tools
-@firefox
qutebrowser
#chromium
#error chromium
libXScrnSaver

openbox
lxpanel
lxappearance
zathura
qt5-qtwebengine
python3-qt5-webengine
#fedora-icon-theme
paper-icon-theme
gvim
emacs
modem-manager-gui
anki
#mpv needs vulkan. otherwise disable both
mpv
mesa-vulkan-drivers
%end
