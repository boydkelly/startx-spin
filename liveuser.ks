%post
#X config stuff
touch /etc/skel/.Xdefaults
ln -sr /etc/skel/.Xdefaults /etc/skel/.Xresources

#We use the command line a lot in fluxbox
cat >> /etc/skel/.bashrc << 'FOE'
if [ -f $(which powerline-daemon) ]; then
	powerline-daemon -q
	POWERLINE_BASH_CONTINUATION=1
	POWERLINE_BASH_SELECT=1
	. /usr/share/powerline/bash/powerline.sh
fi
FOE

%end
