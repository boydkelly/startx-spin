%post --nochroot
#generic stuff
cp /etc/ssmtp/ssmtp.conf $INSTALL_ROOT/etc/ssmtp/
cp /etc/X11/xorg.conf.d/* $INSTALL_ROOT/etc/X11/xorg.conf.d/
cp /etc/xdg/openbox/* $INSTALL_ROOT/etc/skel/.config/openbox/

#personal stuff below
tar -C /home/bkelly -cf - .vimrc .vim .local/share/nvim/site .config/nvim | ( cd $INSTALL_ROOT/etc/skel/; tar -xpf -)
tar -C /home/bkelly -cf - .bashrc .bash_profile | ( cd $INSTALL_ROOT/etc/skel/; tar -xpf -)

mkdir -p $INSTALL_ROOT/etc/skel/.config/i3
cp /home/bkelly/.config/i3/* $INSTALL_ROOT/etc/skel/.config/i3/
mkdir  $INSTALL_ROOT/etc/skel/.config/sway
cp /home/bkelly/.config/sway/* $INSTALL_ROOT/etc/skel/.config/sway/

%end
