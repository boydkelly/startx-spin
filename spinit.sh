#!/usr/bin/bash
export LNAME=${PWD##*/}
export NAME=`echo $LNAME | tr '[:lower:]' '[:upper:]'`
export LOCATION="/mnt/windows/isos"
if [ ! -d /mnt/misc/dnf ]; then
	echo 'test'
fi

sudo livecd-creator -c ${LNAME}.ks -f $NAME \
	--cache=/mnt/misc/dnf -v --releasever=29 \
	--cacheonly

retval=$?
if [ $retval -eq 0 ]; then
	if [ $HOSTNAME = "y2" ]; then
		echo $retval
		echo "We are moving iso!"
		mv -f ${NAME}.iso ${LOCATION}/${LNAME}.iso
		if [ $retval -eq 0 ];then
			rm -f ${NAME}.iso
		fi
		sudo rm -fr /var/tmp/img*
	fi
	echo "`hostname`" | mail -s "Success!  Created ${LNAME}.iso at `date`" bkelly@coastsystems.net
fi

