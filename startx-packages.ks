# fedora-startx.ks
#
# Description:
# - Fedora Live Spin with i3/sway
#
# Maintainer(s):

%packages
#-@*

@base-x
@core
@standard
-@printing
-@fonts
-@input-methods
-@dial-up
-@multimedia
-@firefox
-@hardware-support
-@network-manager-submodules
-@anaconda-tools
-anaconda
-anaconda-install-env-deps
-aajohan-comfort-fonts

-abrt-*
-autofs
-acpid
-aspell-*

#Fedora 29 default font (gnome)
liberation-mono-fonts
liberation-sans-fonts
liberation-serif-fonts
terminus-fonts-console
powerline-fonts

network-manager-applet
NetworkManager-tui
NetworkManager-wifi
nm-tray
ModemManager
wpa_supplicant
iw
wireless-tools

sakura
neovim
emacs-nox
tmux
ranger
powerline
tmux-powerline
f29-backgrounds-base
xorg-x11-xinit
xorg-x11-apps
xterm
xorg-x11-utils
xorg-x11-drv-libinput
xorg-x11-xauth

#error with sway
mesa-libgbm
#error with startx i3
libxshmfence

i3
dbus-x11
sway
i3lock
i3status
dunst
feh
xcompmgr
udiskie
xautolock
scrot

pulseaudio
pulseaudio-utils
pulseaudio-module-x11
blueman

#monitoring
glances
procps-ng

rfkill

wget
curl 
elinks

#mail support
ssmtp
mailx

#passwd error on spin
libffi
#i3status error
alsa-lib
#
libXtst
libSM

%end
