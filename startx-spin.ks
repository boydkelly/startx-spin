# Fedora Spin for Startx System
# http:/www.coastsystems.net
#
# Description
# - Fedora Live Spin with i3, sway & Security Lab!
#
# Maintainer(s):
# - Boyd Kelly       <bkelly AT coastsystems  DOT .net>


%include fedora-live-base.ks
%include fedora-minimal-common.ks
%include fedora-live-minimization.ks

# Custom
%include extrarepos.ks
%include lang.ks

# packages
#%include fluxbox-packages.ks
%include startx-packages.ks
%include gui-packages.ks
%include tui-packages.ks
#%include rescue-packages.ks

# machine specific
%include firmware.ks
%include hidpi.ks
%include kvm-guest.ks

# settings
%include winmount.ks
%include liveuser.ks
%include i3.ks

%include post-nochroot.ks

services --enabled=NetworkManager,ModemManager,serial-getty@ttyS0.service

#
# Disable this for now as packagekit is causing compose failures
# by leaving a gpg-agent around holding /dev/null open.
#
#include snippets/packagekit-cached-metadata.ks

part / --size 6144 --fstype ext4
timezone --utc UTC
selinux --disabled

%post

#/usr/sbin/useradd bkelly
#/usr/bin/passwd -d bkelly > /dev/null
#/usr/sbin/usermod -aG wheel bkelly > /dev/null

cat >> /etc/rc.d/init.d/livesys << EOF

# this goes at the end after all other changes. 
chown -R liveuser:liveuser /home/liveuser
restorecon -R /home/liveuser

EOF

mount -t nfs4 localhost:/bkelly/ /mnt
echo $(date -I)-starx-spin  > /mnt/livecd/install.log

%end
